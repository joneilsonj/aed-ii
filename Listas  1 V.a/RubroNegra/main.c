#include "rn.h"


int main(int argc, char * argv[]){

	
	arvore tree;
	iniciliazar(&tree);
	FILE *arquivo;
	char opcao;
	no *z, *y;
	
	if(argc > 5){
   	exception(argv[0]);
   }
	
	while((opcao = getopt(argc, argv, "ho:f:mMa:s:")) > 0){
		switch(opcao){
			case 'h': // mostra o help
				help(argv[0]);
				break;
				
			case 'o': // redireciona a saida para o ‘‘arquivo’’
				arquivo = fopen(optarg, "wb+");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				salvarArquivo(tree.raiz, arquivo);
				fclose(arquivo);
				break;
				
			case 'f': //indica o ‘‘arquivo’’ que contém os dados a serem adicionados na AVL
				arquivo = fopen(optarg, "rb");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				lerArquivo(&tree, arquivo);
				fclose(arquivo);
				percursoPreOrdem(tree.raiz);
  				printf("\n");
				break;
				
			case 'M': //imprime o maior elemento da avl
				z = treeMax(tree.raiz);
				printf("%d\n", z->dado);
				break;
			case 'm': //imprime o menor elemento da avl
				z = treeMinimum(tree.raiz);
				printf("%d\n", z->dado);
				break;
				
			case 'a': //imprime o antecessor na AVL do ‘‘elemento’’, caso o elemento nao exista na arvore ou seja o minimo imprime -1
				z = treeSearch(&tree, atoi(optarg)); 
				if(z == NULL){
					
					printf("-1\n");
					break;
				}
				y = treeAntecessor(z);
				
				if(y != NULL){
					printf("%d\n", y->dado);
				}else{
					printf("-1\n");
					break;
				}
				break;
			case 's': //imprime o sucessor na AVL do ‘‘elemento’’,caso o elemento nao exista na arvore ou seja o maximo imprime -1
				z = treeSearch(&tree, atoi(optarg));
				if(z == NULL){
					printf("-1\n");
					break;
				}
				y = treeSucessor(z);
				
				if(y != NULL){
					printf("%d\n", y->dado);
				}else{
					printf("-1\n");
					break;
				}
				break;
			
			default:
				return 1;			
		}	
	}
	
	return 0;
}
