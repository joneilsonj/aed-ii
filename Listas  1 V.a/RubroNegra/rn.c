#include "rn.h"

// parte de comentarios igual da AVL.

void iniciliazar(arvore *inicio){
	inicio->raiz = NULL;

}

void percursoEmOrdem(no *x){
	if(x!=NULL){
		percursoEmOrdem(x->esquerdo);
		printf("%d\n", x->dado);
		percursoEmOrdem(x->direito);	
	}
}


void percursoPreOrdem(no *a){
	
	printf("(%d%s ", a->dado, Cor(a));
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			printf("(x)");
		}else{
			printf("(x");
		}
			
	}else{
		percursoPreOrdem(a->esquerdo);
	
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			printf(" (x)");
		}else{
			printf(" x)");
		}
	}else{
		percursoPreOrdem(a->direito);
	}
	
	printf(")");

}


void percursoPosOrdem(no *x){
	if(x!=NULL){
		percursoPosOrdem(x->esquerdo);
		percursoPosOrdem(x->direito);
		printf("%d\n", x->dado);	
	}
}

no* criarNo(int valor){
	no *z = (no *)malloc(sizeof(no));
	z->dado = valor;
	z->pai = NULL;
	z->esquerdo = NULL;
	z->direito = NULL;
	z->cor = 1;
	
	return z;
}

void treeInsert(arvore *tree, int valor){
	no *x, *y, *z;	
	x = tree->raiz;
	y = NULL;
	z = criarNo(valor);
	while(x != NULL){
		y = x;
		if(z->dado < x->dado){
			x = x->esquerdo;
		}else {
			x = x->direito;     
		}
	}
	z->pai = y;
	if(y == NULL){
		tree->raiz = z;
		tree->raiz->cor = 0;
			
	}else if(y->dado > z->dado){
		y->esquerdo = z;
	}else{
		y->direito = z;
	}

	Balancear(tree, z);
}

// cria o tio caso seja um nulo

no* criarTio(int valor){
   no *z = (no *)malloc(sizeof(no));
	z->dado = valor;
	z->pai = NULL;
	z->esquerdo = NULL;
	z->direito = NULL;
	z->cor = 0;
	
	return z;
}

// uma funcao pra poder retornar quem eh o tio, caso seja um nulo
// aloca um no com o valor 0 e cor preta, pra ficar mais facil de nao
// acontecer Falha de segmentacao

no* Tio(no *z){
   if(z->pai != NULL){
      if(z->pai->pai != NULL){
         if(z->pai->pai->direito != z->pai){
            if(z->pai->pai->direito != NULL){
               return z->pai->pai->direito;
            }else{
               return criarTio(0);
            }
         }else{
            if(z->pai->pai->esquerdo != NULL){
               return z->pai->pai->esquerdo;
            }else{
               return criarTio(0);
            }
         }
      }
   }
   return criarNo(0);
   
}

// parte de balanceamento da arvore

void Balancear(arvore *tree, no *z){
   no *u, *tio;   
   
   while(z != NULL ){
   
      if(z->cor == 1 && z->pai->cor == 1){
         tio = Tio(z);	
			
	   if(z->pai->cor == 1 && tio->cor == 1){
		   z->pai->cor = 0;
		   tio->cor = 0;
		   if(tree->raiz == z->pai->pai){
		      z->pai->pai->cor = 0;
		   }else{
		      z->pai->pai->cor = 1;
		   }						
	   }else if(z->pai->cor == 1 && tio->cor == 0){
	   				
            if(z->pai->esquerdo == z && z->pai->pai->esquerdo == z->pai){
            	    rotacao_direito(z->pai->pai);
            	    z->pai->cor = 0;
                    z->pai->direito->cor = 1;
            
            }else if(z->pai->direito == z && z->pai->pai->direito == z->pai){
               rotacao_esquerdo(z->pai->pai);
               z->pai->cor = 0;
               z->pai->esquerdo->cor = 1;
            
            }else if(z->pai->direito == z && z->pai->pai->direito != z->pai){
               rotacao_esquerdo(z->pai);
            
               rotacao_direito(z->pai);
               z->cor = 0;
               z->direito->cor = 1;
               if(z->esquerdo != NULL){
                  z->esquerdo->cor = 0;
               }
            
            }else if(z->pai->esquerdo == z && z->pai->pai->esquerdo != z->pai){
               rotacao_direito(z->pai);
            
               rotacao_esquerdo(z->pai);
               z->cor = 0;
               z->esquerdo->cor = 1;
               if(z->direito != NULL){
                  z->direito->cor = 0;
               }
            }			
	    }
	}
    if(z->pai == NULL){
	tree->raiz = z;
	tree->raiz->cor = 0;
    }
    z=z->pai;
		
    }

}

// parte de comentarios igual da AVL.

no* treeSearch(arvore *tree, int valor){
	no* x;
	
	x = tree->raiz;
	while(x != NULL && x->dado != valor){
		if(x->dado > valor){
			x = x->esquerdo;
		}else{
			x = x->direito;
		}
	}
	
	if(x == NULL){
		return NULL;
	}else{
		return x;
	}
}

no* treeMinimum(no *x){
	while(x->esquerdo != NULL && x != NULL){
		x = x->esquerdo;
	}
	
	return x;
}

no* treeSucessor(no *x){
	no *y;
	if(x->direito != NULL){
		return treeMinimum(x->direito);
	}
	
	y = x->pai;
	while(y != NULL && x == y->direito){
		x = y;
		y = y->pai;
	}	
	return y;
}

no* treeAntecessor(no *x){
   no *y;
   if(x->esquerdo != NULL){
      return treeMax(x->esquerdo);
      
   }
   
   y = x->pai;
   while(y != NULL && x == y->esquerdo){
      x = y;
      y = y->pai;
   }
   return y;

}

no* treeMax(no *x){
   while(x->direito != NULL && x != NULL){
      x = x->direito;
   }
   return x;

}

int max(int esquerdo, int direito){

	if(esquerdo > direito){
		return esquerdo;

	}else{
		return direito;
	}
}


int calcular_altura(no *x){
	int h, altura_esquerdo, altura_direito;
	
	if (x == NULL){
		return 0;
	}
	else{
      altura_direito = calcular_altura(x->direito);
      altura_esquerdo = calcular_altura(x->esquerdo);	
		h = max(altura_esquerdo, altura_direito);
		
		return h+1;
	}
}

void rotacao_direito(no *x){
	no *y = x->esquerdo;
	no *y_direito = y->direito;
	
	y->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->direito){
		   x->pai->direito = y;
		}else{
		   x->pai->esquerdo = y;
		}
	}
	y->direito = x;
	x->pai = y;
	x->esquerdo = y_direito;
	if(y_direito != NULL){
		y_direito->pai = x;
	}
	
}

void rotacao_esquerdo(no *x){
	
	no *y = x->direito;
	no *y_esquerdo = y->esquerdo;
	
	y->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->direito){
		   x->pai->direito = y;
		}else{
		   x->pai->esquerdo = y;
		}
	}
	y->esquerdo = x;
	x->pai = y;
	x->direito = y_esquerdo;
	if(y_esquerdo != NULL){
		y_esquerdo->pai = x;
	}
		
}

// retorna um char dizendo se o no eh preto ou vermelho
// pra facilitar os prints e saves em arquivos

char* Cor(no *a){
	if(a->cor == 0){
		return "N";
	}else{
		return "R";
	}
}

// parte que salva, le, menu e trata erros

void salvarArquivo(no *a, FILE *arquivo){
	
	fprintf(arquivo, "(%d%s ", a->dado, Cor(a));
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			fprintf(arquivo, "(x)");
		}else{
			fprintf(arquivo, "(x");
		}
			
	}else{
		salvarArquivo(a->esquerdo, arquivo);
	
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			fprintf(arquivo, " (x)");
		}else{
			fprintf(arquivo, "x)");
		}
	}else{
		salvarArquivo(a->direito, arquivo);
	}
	
	fprintf(arquivo, ")");
	
}

void lerArquivo(arvore *tree, FILE *arquivo){
	int i;
	
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		treeInsert(tree, i);
	}

}

void help(char *name){
	printf("-h                     : mostra o help\n");
	printf("-o <arquivo>           : redireciona a saida para o ‘‘arquivo’’\n");
	printf("-f <arquivo>           : indica o ‘‘arquivo’’ que contém os dados a serem adicionados na RubroNegra\n");
	printf("-m                     : imprime o menor elemento da RubroNegra\n");
	printf("-M                     : imprime o maior elemento da RubroNegra\n");
	printf("-a <elemento>          : imprime o antecessor na RubroNegra do ‘‘elemento’’, caso o elemento nao esteja na arvore, ou seja o minimo imprime -1\n");
	printf("-s <elemento>          : imprime o sucessor na RubroNegra do ‘‘elemento’’, caso o elemento nao esteja na arvore, ou seja o maximo imprime -1\n");
	exit(-1);
}

void exception(char *name){
	printf("ERRO: parâmetros conflitantes \n");
	exit(-1);
}

