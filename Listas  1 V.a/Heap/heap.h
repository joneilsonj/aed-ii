#ifndef _HEAP_H
#define _HEAP_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct heap{
   int *a;
   int tamanho;
   int comprimento;   

}heap;

int inserir(heap *h, int valor);
void inicializar(heap *h, int t);
int pai(int i);
int esquerdo(int i);
int direito(int i);
void trocar(heap *h, int i, int  maior);
void help(char *name);
void exception(char *name);

void maxHeapify(heap *h, int i);
void buildMaxHeap(heap *h);
void maxHeapSort(heap *h);

void minHeapify(heap *h, int i);
void buildMinHeap(heap *h);
void minHeapSort(heap *h);

void mostrarMax(heap *h);
void mostrarMin(heap *h);

void salvarArquivoMin(heap *h, FILE *arquivo);
void salvarArquivoMax(heap *h, FILE *arquivo);
void lerArquivo(heap *h, FILE *arquivo);



#endif
