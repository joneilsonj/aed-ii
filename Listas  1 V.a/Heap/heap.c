#include "heap.h"

// Inicializa o heap

void inicializar(heap *h, int t){
   h->a = (int* )malloc(sizeof(int)*(t+1));
   h->tamanho = 0;
   h->comprimento = t;
   
}

// Insere um inteiro no heap

int inserir(heap *h, int valor){
   if(h->tamanho == h->comprimento){
      return -1;
   }
   h->tamanho++;
   h->a[h->tamanho] = valor;
	return 0;
}

// calcula pai, filho esquerdo e filho direito

int pai(int i){
   return (i/2);
 
}

int esquerdo(int i){
   return (i*2);
   
}

int direito(int i){
   return ((i*2)+1);
   
}

// troca dois valores, dado o indice

void trocar(heap *h, int i, int  maior){
   int aux;
   aux = h->a[maior];
   h->a[maior] = h->a[i];
   h->a[i] = aux;
  
}

// parte que le, menu e tratamento de erros

void lerArquivo(heap *h, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		inserir(h, i);
	}
}

void help(char *name){
	printf("-h                     : mostra o help\n");
	printf("-o <arquivo>           : redireciona a saida para o ‘‘arquivo’’\n");
	printf("-f <arquivo>           : indica o ‘‘arquivo’’ que contém os dados a serem adicionados na Heap\n");
	printf("-m                     : indica que a estrutura será uma heap mínima\n");
	exit(-1);
}

void exception(char *name){
	printf("ERRO: parâmetros conflitantes \n");
	exit(-1);
}

//              funcoes do:

//=============HEAP DE MINIMO==================

void minHeapify(heap *h, int i){
   int l, r, menor;
   l = esquerdo(i);
   r = direito(i);
   
   if(l <= h->tamanho && h->a[l] > h->a[i]){
      menor = l;
   }else{
      menor = i;
   }
   if(r <= h->tamanho && h->a[r] > h->a[menor]){
      menor = r;
   }
   if(menor != i){
      trocar(h, i, menor);
      minHeapify(h, menor);
   }
}

void buildMinHeap(heap *h){
   int i;
   
   h->tamanho = h->comprimento;
   
   for(i = (h->comprimento/2); i>0; i--){
      minHeapify(h, i);
   }
   
}

void minHeapSort(heap *h){
   int i, tamanho;
   tamanho = h->tamanho;
   
   buildMinHeap(h);
   
   for(i = h->comprimento; i>1; i--){
      trocar(h, 1, i);
      h->tamanho= h->tamanho-1;
      minHeapify(h, 1);
   
   }
   h->tamanho = tamanho;
   
}

void mostrarMin(heap *h){
   int i;
   
   for(i = (h->comprimento - h->tamanho)+1; i <= h->comprimento; i++){
      printf("%d ", h->a[i]);
   }
   printf("\n");
}

void salvarArquivoMin(heap *h, FILE *arquivo){
	int i;
	
	for(i = (h->comprimento - h->tamanho) +1; i<=h->comprimento; i++){
		fprintf( arquivo, "%d ", h->a[i]);
	} 

}


//=============HEAP DE MAXIMO===============

void maxHeapify(heap *h, int i){
   int l, r, maior;
   l = esquerdo(i);
   r = direito(i);
   
   if(l <= h->tamanho && h->a[l] < h->a[i]){
      maior = l;
   }else{
      maior = i;
   }
   if(r <= h->tamanho && h->a[r] < h->a[maior]){
      maior = r;
   }
   if(maior != i){
      trocar(h, i, maior);
      maxHeapify(h, maior);
   }
}

void buildMaxHeap(heap *h){
   int i;
   
   h->tamanho = h->comprimento;
   
   for(i = (h->comprimento/2); i>0; i--){
      maxHeapify(h, i);
   }
   
}

void maxHeapSort(heap *h){
   int i, tamanho;
   tamanho = h->tamanho;
   
   buildMaxHeap(h);
   
   for(i = h->comprimento; i>1; i--){
      trocar(h, 1, i);
      h->tamanho= h->tamanho-1;
      maxHeapify(h, 1);
   
   }
   h->tamanho = tamanho;
   
}

void mostrarMax(heap *h){
   int i;
   
   for(i = 1; i <= h->tamanho; i++){
      printf("%d ", h->a[i]);
   }
   printf("\n");
}

void salvarArquivoMax(heap *h, FILE *arquivo){
	int i;
	
	for(i = 1; i<=h->tamanho; i++){
		fprintf(arquivo, "%d ", h->a[i]);
	}

}







