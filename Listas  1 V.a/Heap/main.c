#include "heap.h"


int main(int argc, char * argv[]){

	heap h;
	inicializar(&h, 12);
	FILE *arquivo;
	char opcao;
	int a;
	a = 0;
	if(argc > 6){
   	exception(argv[0]);
   	}
	
	while((opcao = getopt(argc, argv, "ho:f:mMa:s:")) > 0){
		switch(opcao){
			case 'h': // mostra o help
				help(argv[0]);
				break;
				
			case 'o': // redireciona a saida para o ‘‘arquivo’’
				arquivo = fopen(optarg, "wb+");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				if(a == 0){ 
					salvarArquivoMax(&h, arquivo);
				}else{
					salvarArquivoMin(&h, arquivo);
				}
				fclose(arquivo);
				break;
				
			case 'f': //indica o ‘‘arquivo’’ que contém os dados a serem adicionados na AVL
				arquivo = fopen(optarg, "rb");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				lerArquivo(&h, arquivo);
				fclose(arquivo);
				if(a == 0){
					maxHeapSort(&h);
					mostrarMax(&h);
  					printf("\n");
  				}else{
  					minHeapSort(&h);
  					mostrarMin(&h);
  					printf("\n");
  				}
				break;
			case 'm':
				a = 1;
		}
	}
	return 0;

}
