#include "avl.h"

//Inicializa a arvore
void iniciliazar(arvore *inicio){
	inicio->raiz = NULL;

}

//prints em percursos ========================================
void percursoEmOrdem(no *x){
	if(x!=NULL){
		percursoEmOrdem(x->esquerdo);
		printf("%d\n", x->dado);
		percursoEmOrdem(x->direito);	
	}
}

//=================com saida da maneira de uma arvore ==============
void percursoPreOrdem(no *a){
	printf("(%d ", a->dado);
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			printf("(x)");
		}else{
			printf("(x ");
		}
			
	}else{
		percursoPreOrdem(a->esquerdo);
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			printf(" (x)");
		}else{
			printf(" x)");
		}
	}else{
		percursoPreOrdem(a->direito);
	}
	printf(")");

}


void percursoPosOrdem(no *x){
	if(x!=NULL){
		percursoPosOrdem(x->esquerdo);
		percursoPosOrdem(x->direito);
		printf("%d\n", x->dado);	
	}
}

//===========Aloca Um no dado um determinado valor

no* criarNo(int valor){
	no *z = (no *)malloc(sizeof(no));
	z->dado = valor;
	z->pai = NULL;
	z->esquerdo = NULL;
	z->direito = NULL;
	
	return z;
}


// insere na arvore e chama a funcao de balanceamento
void treeInsert(arvore *tree, int valor){
	no *x, *y, *z;	
	x = tree->raiz;
	y = NULL;
	z = criarNo(valor);
	while(x != NULL){
		y = x;
		if(z->dado < x->dado){
			x = x->esquerdo;
		}else {
			x = x->direito;
		}
	}
	z->pai = y;
	if(y == NULL){
		tree->raiz = z;	
	}else if(y->dado > z->dado){
		y->esquerdo = z;
	}else{
		y->direito = z;
	}

	Balancear(tree, z);
}

//balanceia a arvore

void Balancear(arvore *tree, no *z){
   no *u;   
   
   while(z != NULL ){		
		if (calcular_altura(z->esquerdo) - calcular_altura(z->direito) >= 2){
			u = z->esquerdo;
			
			if(calcular_altura(u->esquerdo) > calcular_altura (u->direito)){
				rotacao_direito(z);
			}else{
				dupla_rotacao_direito(z);

			}
			
		}if(calcular_altura(z->direito) - calcular_altura(z->esquerdo) >= 2){				
			u = z->direito;
			
			if(calcular_altura(u->direito) > calcular_altura (u->esquerdo)){
			
				rotacao_esquerdo(z);

			}else{
				dupla_rotacao_esquerdo(z);
			
			}
		}
		if(z->pai == NULL){
			tree->raiz = z;
			
		}
		z=z->pai;
		
	}

}

// procura se existe um valor na arvore

no* treeSearch(arvore *tree, int valor){
	no *x;
	x = tree->raiz;
	
	while(x != NULL && x->dado != valor){
		if(x->dado > valor){
			x = x->esquerdo;
		}else{
			x = x->direito;
		}
	}
	
	if(x == NULL){
		return NULL;
	}else{
		return x;
	}
	
	
}

// procura o minino dado um determinado no

no* treeMinimum(no *x){
	while(x->esquerdo != NULL && x != NULL){
		x = x->esquerdo;
	}
	
	return x;
}

// procura o sucessor de um determinado no 

no* treeSucessor(no *x){
	no *y;
	if(x->direito != NULL){
		return treeMinimum(x->direito);
	}
	
	y = x->pai;
	while(y != NULL && x == y->direito){
		x = y;
		y = y->pai;
	}	
	return y;

}

// procura o antecessor de um no

no* treeAntecessor(no *x){
   no *y;
   if(x->esquerdo != NULL){
      return treeMax(x->esquerdo);
      
   }
   
   y = x->pai;
   while(y != NULL && x == y->esquerdo){
      x = y;
      y = y->pai;
   }
   return y;

}

// procura o maximo dado um determinado no

no* treeMax(no *x){
   while(x->direito != NULL && x != NULL){
      x = x->direito;
   }
   return x;

}

// retorna qual o maior, dado dois inteiros

int max(int esquerdo, int direito){

	if(esquerdo > direito){
		return esquerdo;

	}else{
		return direito;
	}
}

//calcula a altura de um no

int calcular_altura(no *x){
	int h, altura_esquerdo, altura_direito;
	
	if (x == NULL){
		return 0;
	}
	else{
        	altura_direito   = calcular_altura(x->direito);
        	altura_esquerdo  = calcular_altura(x->esquerdo);	
		h = max(altura_esquerdo, altura_direito);
		
		return h+1;
	}
}

// =======================rotacoes======================

void rotacao_direito(no *x){
	no *y = x->esquerdo;
	no *y_direito = y->direito;
	
	y->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->direito){
		   x->pai->direito = y;
		}else{
		   x->pai->esquerdo = y;
		}
	}
	y->direito = x;
	x->pai = y;
	x->esquerdo = y_direito;
	if(y_direito != NULL){
		y_direito->pai = x;
	}
	
}

void rotacao_esquerdo(no *x){
	
	no *y = x->direito;
	no *y_esquerdo = y->esquerdo;
	
	y->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->direito){
		   x->pai->direito = y;
		}else{
		   x->pai->esquerdo = y;
		}
	}
	y->esquerdo = x;
	x->pai = y;
	x->direito = y_esquerdo;
	if(y_esquerdo != NULL){
		y_esquerdo->pai = x;
	}
		
}


void dupla_rotacao_direito(no *x){
	rotacao_esquerdo(x->esquerdo);
	rotacao_direito(x);

}

void dupla_rotacao_esquerdo(no *x){
	rotacao_direito(x->direito);
	rotacao_esquerdo(x);
}

// parte de arquivo e correcao de erros

void salvarArquivo(no *a, FILE *arquivo){
	
	fprintf(arquivo, "(%d ", a->dado);
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			fprintf(arquivo, "(x)");
		}else{
			fprintf(arquivo, "(x ");
		}
			
	}else{
		salvarArquivo(a->esquerdo, arquivo);
	
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			fprintf(arquivo, " (x)");
		}else{
			fprintf(arquivo, " x)");
		}
	}else{
		salvarArquivo(a->direito, arquivo);
	}
	
	fprintf(arquivo, ")");

}

void lerArquivo(arvore *tree, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		treeInsert(tree, i);
	}

}

void help(char *name){
	printf("-h                     : mostra o help\n");
	printf("-o <arquivo>           : redireciona a saida para o ‘‘arquivo’’\n");
	printf("-f <arquivo>           : indica o ‘‘arquivo’’ que contém os dados a serem adicionados na AVL\n");
	printf("-m                     : imprime o menor elemento da AVL\n");
	printf("-M                     : imprime o maior elemento da AVL\n");
	printf("-a <elemento>          : imprime o antecessor na AVL do ‘‘elemento’’, caso o elemento nao esteja na arvore, ou seja o minimo imprime -1\n");
	printf("-s <elemento>          : imprime o sucessor na AVL do ‘‘elemento’’, caso o elemento nao esteja na arvore, ou seja o maximo imprime -1\n");
	exit(-1);
}

void exception(char *name){
	printf("ERRO: parâmetros conflitantes \n");
	exit(-1);
}
