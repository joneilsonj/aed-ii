#ifndef _AVL_H
#define _AVL_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct no{
	struct no *pai;
	struct no *esquerdo;
	struct no *direito;	
	int dado;

}no;

typedef struct arvore{
	no *raiz;

}arvore;

void iniciliazar(arvore *inicio);
void percursoEmOrdem(no *x);
void percursoPreOrdem(no *x);
void percursoPosOrdem(no *x);
no* criarNo(int valor);
void treeInsert(arvore *tree, int valor);
no* treeSearch(arvore *tree, int valor);
no* treeMinimum(no *x);
no* treeSucessor(no *x);
no* treeMax(no *x);
no* treeAntecessor(no *x);
int calcular_altura(no *x);
int max(int esquerdo, int direito);
void Balancear(arvore *tree, no *z);
void rotacao_direito(no *x);
void rotacao_esquerdo(no *x);
void dupla_rotacao_esquerdo(no *x);
void dupla_rotacao_direito(no *x);
void salvarArquivo(no *a, FILE *arquivo);
void lerArquivo(arvore *tree, FILE *arquivo);
void help(char *name);
void exception(char *name);
#endif
