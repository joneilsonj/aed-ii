#include "hash.h"

// inicia o hash

void inicializar(hash *h, int tamanho){
   h->lista = (no* *)malloc(sizeof(no*)*(tamanho));
   h->tamanho = tamanho;

}

// funcao do hash, que determina para onde cada chave sera direcionada
// de forma que "balanceia" o hash 

int H(hash *h, int chave){
   return (chave % h->tamanho);
}  

no* criarNo(int valor){
   no *z = (no *)malloc(sizeof(no));
   z->proximo = NULL;
   z->dado = valor;
}

// insere um determinado inteiro no hash

void hashInsert(hash *h, int valor){
   no *z, *temp, *y;
   y = NULL;
   z = criarNo(valor);
   temp = h->lista[H(h, valor)];
   
   while(temp != NULL){
      y = temp;
      temp = temp->proximo;
   }
   
   if(y == NULL){
      h->lista[H(h, valor)] = z;
   }else{
      y->proximo = z;
   }
      
}

// busca no hash

no* hashSearch(hash *h, int valor){
   no *temp;
   
   temp = h->lista[H(h, valor)];
   while(temp != NULL){
      if(temp->dado == valor){
         return temp;   
      }
      temp = temp->proximo;
   }
   return temp;
  
}

// imprime todo o hash de forma de boa visualizacao

void imprimeHash(hash *h){
   int i;
   no *temp;
   
   for(i = 0; i < h->tamanho; i++){
      temp = h->lista[i];
      printf("%d: ", i);
      while(temp != NULL){
         printf("%d ", temp->dado);
         temp = temp->proximo;
      }
      
      printf("\n");
   }
}

// funcoes de salvar, ler, menu e tratamento de erros

void salvarArquivo(hash *h, FILE *arquivo){
	
   int i;
   no *temp;
   
   for(i = 0; i < h->tamanho; i++){
      temp = h->lista[i];
      fprintf(arquivo, "%d: ", i);
      while(temp != NULL){
         fprintf(arquivo, "%d ", temp->dado);
         temp = temp->proximo;
      }
      fprintf(arquivo, "\n");
   }


}

void lerArquivo(hash *h, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
	
		hashInsert(h, i);
	}

}

void help(char *name){
	printf("-h                     : mostra o help\n");
	printf("-o <arquivo>           : redireciona a saida para o ‘‘arquivo’’\n");
	printf("-f <arquivo>           : indica o ‘‘arquivo’’ que contém os dados a serem adicionados na Hash\n");
	printf("-m                     : o tamanho da hash (default=11)\n");
	exit(-1);
}

void exception(char *name){
	printf("ERRO: parâmetros conflitantes \n");
	exit(-1);
}


