#ifndef _HASH_H
#define _HASH_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>


typedef struct no{
   int dado;
   struct no *proximo;
}no;

typedef struct hash{
   int tamanho;
   no* *lista;

}hash;

void inicializar(hash *h, int tamanho);
int h(hash *h, int chave);
no* criarNo(int valor);
void hashInsert(hash *h, int valor);
no* hashSearch(hash *h, int valor);
void imprimeHash(hash *h);

void salvarArquivo(hash *h, FILE *arquivo);
void lerArquivo(hash *h, FILE *arquivo);

void help(char *name);
void exception(char *name);

#endif
