#include "hash.h"

int main(int argc, char * argv[]){

	hash h;
	
	FILE *arquivo;
	char opcao;
	int tamanho;
	tamanho = 11;
	if(argc > 7){
   	exception(argv[0]);
  	}		
	
	while((opcao = getopt(argc, argv, "ho:f:mMa:s:")) > 0){
		switch(opcao){
			case 'h': // mostra o help
				help(argv[0]);
				break;
				
			case 'o': // redireciona a saida para o ‘‘arquivo’’
			
				arquivo = fopen(optarg, "wb+");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				salvarArquivo(&h, arquivo);
				fclose(arquivo);
				break;
				
			case 'f': //indica o ‘‘arquivo’’ que contém os dados a serem adicionados na AVL
				
				inicializar(&h, tamanho);
				arquivo = fopen(optarg, "rb");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				lerArquivo(&h, arquivo);
				fclose(arquivo);
				imprimeHash(&h);
  				printf("\n");
				break;
			case 'm': // nao funcionando!!!!!!!!!
				
				tamanho = atoi(argv[2]);
				break;
				
		}
	}
	return 0;
	

}
