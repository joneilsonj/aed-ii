#ifndef _PRIM_H
#define _PRIM_H


#include <string.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#define MAX 1000

typedef struct{
	int verticeO;
	int verticeD;
    int peso;
}Aresta;


struct no{
	Aresta* aresta;
	struct no *prox;	
};


typedef struct Grafo{	
	int qtdArestas;
	int qtdVertices;
	int countA;
	struct no* vetorArestas[MAX];
	
}Grafo;

typedef struct{
    int contador;
    Aresta* vetor[MAX];
}VetorAresta;

typedef struct{
    int contador;
    int vetor[MAX];
}VetorVertice;

void comecarVetorArestas(VetorAresta *vetorAresta, int valor);
void comecarVetorVertices(VetorVertice *vetor);
Aresta* comecarAresta(int verticeO, int verticeD, int peso1);
struct no* comecarNo(int verticeO, int verticeD, int peso);
void comecarGrafo(Grafo *grafo, int qtdArestas, int qtdVertices);
void selectionSort(VetorAresta *vetorAresta);
void selectionSortArestas(VetorAresta *vetorAresta);
void adicionarArestaVetor (VetorAresta *vetorAresta, Aresta *aresta);
void adicionarVertice(VetorVertice *vetorVertice, int numero);
int adicionarVerticeCondicao(VetorVertice *vetorVerticeI, VetorVertice *vetorVerticeF, int verticeO, int verticeD);
int adicionarAresta(Grafo *grafo, int verticeO, int verticeD, int peso);
void removerVertice(VetorVertice *vetorVertice, int indice);
void removerAresta(VetorAresta *vetorAresta, int verticeO, int verticeD);
void criarVetorArestas(Grafo *grafo, VetorAresta *vetorAresta);
void criarVetorVertices(Grafo *grafo, VetorVertice *vetorVertice);
int verificacaoDeVertice1(VetorVertice *vetorVertice, int verticeO, int verticeD);;
int verificacaoDeVertice2(VetorVertice *vetorVertice, int verticeO, int verticeD);
void Prim(Grafo *grafoI, Grafo *grafoF, VetorAresta *vetorArestaF);
void mostrarVetorVertice(VetorVertice *vetor);
void mostrarVetorArestas(VetorAresta *vetorAresta);
void mostrarGrafo(Grafo *grafo);
int calcularPesoTotal(VetorAresta *vetorAresta);;
void entradaTXT(Grafo *grafo, char arquivo[]);
void saidaTXT(VetorAresta *vetorAresta, FILE *arquivo);

#endif