#include "prim.h"

int main(int argv, char *argc[]){
    Grafo grafoInicial;
    Grafo grafoFinal;
    VetorAresta vetorAresta;    
    int i;
    int flag = 0;
    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                    -  Menu de ajuda\n");
            printf("\n-f Recebe arquivo.dat -  Arquivo de entrada\n");
            printf("\n-o Recebe arquivo.dat -  Arquivo de saida\n" );
            printf("\n-s Recebe inteiro     -  mostra a solução (em ordem crescente)\n");
        }
        if(strcmp(argc[i], "-f")== 0){
            entradaTXT(&grafoInicial, argc[i+1]);
            comecarGrafo(&grafoFinal, grafoInicial.qtdArestas, grafoInicial.qtdVertices - 1);
            Prim(&grafoInicial, &grafoFinal, &vetorAresta);
        	flag = 1;
        }
        if(strcmp(argc[i], "-o") == 0){
            FILE *arquivo = fopen(argc[i+1], "w");
            if (arquivo == NULL){
                printf("ERRO - Arquivo de saida invalido\n");
            }
            else{
                saidaTXT(&vetorAresta, arquivo);
            }
            flag = 1; 
        }
        if(strcmp(argc[i], "-s")== 0){
            mostrarVetorArestas(&vetorAresta);        
            flag = 0;
        }
    }
    if(flag == 1){
    	int peso = 0;
        int j;
        for(j = 0;j < vetorAresta.contador; j++ ){
            peso += vetorAresta.vetor[j]->peso;
        }
        printf("%d\n", peso);
    }
    return 0;
}

