#include "prim.h" 


void comecarVetorArestas(VetorAresta *vetorAresta, int valor){
    vetorAresta->contador = valor;

}

void comecarVetorVertices(VetorVertice *vetor){
	vetor->contador = 0;
}

Aresta* comecarAresta(int verticeO, int verticeD, int peso1){
    Aresta *aresta1;
    aresta1 = (Aresta *)malloc(sizeof(Aresta));
    aresta1->verticeO = verticeO;
    aresta1->verticeD = verticeD;
    aresta1->peso = peso1;
    return aresta1;
}

struct no* comecarNo(int verticeO, int verticeD, int peso){
	Aresta *aresta = comecarAresta(verticeO, verticeD, peso);
	struct no *novoNo = (struct no *)malloc(sizeof(struct no));
	novoNo->prox = NULL;
	novoNo->aresta = aresta;
	return novoNo;
}

void comecarGrafo(Grafo *grafo, int qtdArestas, int qtdVertices){
	grafo->qtdVertices = qtdVertices;
	grafo->qtdArestas = qtdArestas;
	grafo->countA = 0;
	int i;
	for(i = 0; i <= qtdVertices; i++){
		grafo->vetorArestas[i] = NULL;
	}
}


void selectionSort(VetorAresta *vetorAresta){
    int i, j, minimo;
    Aresta *temp;
    for(i = 0; i < vetorAresta->contador - 1; i++){
        minimo = i;
        for(j = i+1; j < vetorAresta->contador; j++){
            if(vetorAresta -> vetor[j] -> peso < vetorAresta -> vetor[minimo] -> peso){
                minimo = j;
            }
        }
        if(i != minimo){
            temp = vetorAresta -> vetor[i];
            vetorAresta -> vetor[i] = vetorAresta -> vetor[minimo];
            vetorAresta -> vetor[minimo] = temp;
        }
    }
}

void selectionSortArestas(VetorAresta *vetorAresta){
    int i, j, minimo;
    Aresta *temp;
    for(i = 0; i < vetorAresta->contador - 1; i++){
        minimo = i;
        for(j = i+1; j < vetorAresta->contador; j++){
            if(vetorAresta -> vetor[j] -> verticeO < vetorAresta -> vetor[minimo] -> verticeO){
                minimo = j;
            }
        }
        if(i != minimo){
            temp = vetorAresta -> vetor[i];
            vetorAresta -> vetor[i] = vetorAresta -> vetor[minimo];
            vetorAresta -> vetor[minimo] = temp;
        }
    }
}


void adicionarArestaVetor (VetorAresta *vetorAresta, Aresta *aresta){
	vetorAresta->vetor[vetorAresta->contador] = aresta;
	vetorAresta->contador++;
}

void adicionarVertice(VetorVertice *vetorVertice, int numero){
	vetorVertice->vetor[vetorVertice->contador] = numero;
	vetorVertice->contador++;
}

int adicionarVerticeCondicao(VetorVertice *vetorVerticeI, VetorVertice *vetorVerticeF, int verticeO, int verticeD){
	int i, j;
	int origem = 0;
	int destino = 0;
	for(i = 0; i < vetorVerticeI->contador; i++){
		if(vetorVerticeI->vetor[i] == verticeO){
			origem = -1;
		}
	}

	for(i = 0; i < vetorVerticeI->contador; i++){
		if(vetorVerticeI->vetor[i] == verticeD){
			destino = -1;
		}
	}
	if(origem == -1 && destino == 0){
		vetorVerticeF->vetor[vetorVerticeF->contador] = verticeD;
		vetorVerticeF->contador++;
	}
	if (destino == -1 && origem == 0){
		vetorVerticeF->vetor[vetorVerticeF->contador] = verticeO;
		vetorVerticeF->contador++;
	}
}

int adicionarAresta(Grafo *grafo, int verticeO, int verticeD, int peso){
	struct no *no1;
    no1 = comecarNo(verticeO, verticeD, peso);
    struct no *temp = grafo->vetorArestas[verticeO];
    no1->prox = temp;
    temp = no1;
    grafo->vetorArestas[verticeO] = no1;
    grafo->countA++;
}

void removerVertice(VetorVertice *vetorVertice, int indice){
		int i, j;
		for(i = 0; i < vetorVertice->contador; i++){
			if(vetorVertice->vetor[i] == indice){
				for(j = i; j < vetorVertice->contador - 1; j++){
					vetorVertice->vetor[j] = vetorVertice->vetor[j + 1];
				}
				vetorVertice->contador--;
				break;
			}
		}
	}

void removerAresta(VetorAresta *vetorAresta, int verticeO, int verticeD){
    int i, j;
    for(i = 0; i < vetorAresta -> contador; i++){
        if(vetorAresta -> vetor[i] -> verticeO == verticeO && vetorAresta -> vetor[i] -> verticeD == verticeD){
            for(j = i; j < vetorAresta -> contador - 1; j++){
                vetorAresta -> vetor[j] = vetorAresta -> vetor[j+1];
            }
        vetorAresta -> contador--;
        break;
        }
    }
}



void criarVetorArestas(Grafo *grafo, VetorAresta *vetorAresta){
	int i;
	for(i = 0; i <= grafo->qtdVertices; i++){
		struct no *temp = grafo->vetorArestas[i];
		while(temp != NULL){
			vetorAresta->vetor[vetorAresta->contador] = temp->aresta;
			vetorAresta->contador++;
			temp = temp->prox;
		}
	}
	selectionSort(vetorAresta);
}

void criarVetorVertices(Grafo *grafo, VetorVertice *vetorVertice){
	int i;
	for(i = 0; i <= grafo->qtdVertices; i++){
		vetorVertice->vetor[vetorVertice->contador] = i;
		vetorVertice->contador++;
	}
}



//Ambos os vertices estao no vetor de vertice
int verificacaoDeVertice1(VetorVertice *vetorVertice, int verticeO, int verticeD){
	int i, j;
	int resultado = 0;
	for(i = 0; i < vetorVertice->contador; i++){
		if(vetorVertice->vetor[i] == verticeO){
			for(j = 0; j < vetorVertice->contador; j++){
				if(vetorVertice->vetor[j] == verticeD){
					resultado = -1;
				}
			}
		}
	}
	return resultado;
}

	//ambos os vertices nao estao no vetor de vertice
int verificacaoDeVertice2(VetorVertice *vetorVertice, int verticeO, int verticeD){
	int i, j;
	int flag1 = 0;
	int flag2 = 0;
	int resultado = 0;
	for(i = 0; i < vetorVertice->contador; i++){
		if(vetorVertice->vetor[i] == verticeO){
			flag1 = -1;
		}
	}

	for(i = 0; i < vetorVertice->contador; i++){
		if(vetorVertice->vetor[i] == verticeD){
			flag2 = -1;
		}
	}
	if(flag1 == 0 && flag2 == 0){
		resultado = -1;
	}
	return resultado;

}


void Prim(Grafo *grafoI, Grafo *grafoF, VetorAresta *vetorArestaF){
	VetorAresta vetorAresta;
	VetorVertice vetorVerticeI, vetorVerticeF;
	comecarVetorArestas(&vetorAresta, 0);
	comecarVetorVertices(&vetorVerticeI);
	comecarVetorVertices(&vetorVerticeF);
	criarVetorArestas(grafoI, &vetorAresta);
	criarVetorVertices(grafoI, &vetorVerticeI);
	int j;
	int variavel = 0;
	for(j = 0; j <= vetorAresta.contador; j++){
		if(grafoF->countA == 0){
			adicionarAresta(grafoF, vetorAresta.vetor[0]->verticeO, vetorAresta.vetor[0]->verticeD, vetorAresta.vetor[0]->peso);
			adicionarArestaVetor(vetorArestaF, vetorAresta.vetor[0]);
			removerVertice(&vetorVerticeI, vetorAresta.vetor[0]->verticeD);
			removerVertice(&vetorVerticeI, vetorAresta.vetor[0]->verticeO);
			adicionarVertice(&vetorVerticeF, vetorAresta.vetor[0]->verticeO);
			adicionarVertice(&vetorVerticeF, vetorAresta.vetor[0]->verticeD);
			removerAresta(&vetorAresta, vetorAresta.vetor[0]->verticeO, vetorAresta.vetor[0]->verticeD);
		}
		else if(grafoF->countA == grafoI->qtdVertices - 1){
			break;
		}
		else{
			int i;
			for(i = 0; i < vetorAresta.contador; i++){
				int ambosVerticesNoVetor = verificacaoDeVertice1(&vetorVerticeF, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD);
				int ambosVerticesForaVetor = verificacaoDeVertice2(&vetorVerticeF, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD);
				if(vetorVerticeI.contador == 0){
					break;
				}
				else{
					if(ambosVerticesForaVetor == -1){
						 	variavel++;
					}
					else if(ambosVerticesNoVetor == -1){
						removerAresta(&vetorAresta, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD);
						variavel = 0;	
					
					}
					else{
						adicionarAresta(grafoF, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD, vetorAresta.vetor[variavel]->peso);	
						adicionarArestaVetor(vetorArestaF, vetorAresta.vetor[variavel]);
						removerVertice(&vetorVerticeI, vetorAresta.vetor[variavel]->verticeO);
						removerVertice(&vetorVerticeI, vetorAresta.vetor[variavel]->verticeD);
						adicionarVerticeCondicao(&vetorVerticeF, &vetorVerticeF, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD);
						removerAresta(&vetorAresta, vetorAresta.vetor[variavel]->verticeO, vetorAresta.vetor[variavel]->verticeD);
						variavel = 0;	
					}
				}
			}
		}
	}

}

void mostrarVetorVertice(VetorVertice *vetor){
	int i;
	for(i = 0; i<vetor->contador; i++){
		printf("%d - ", vetor->vetor[i]);
	}
	printf("\n");
}

void mostrarVetorArestas(VetorAresta *vetorAresta){
    int i;
    for(i = 0; i < vetorAresta->contador; i++ ){
        printf("(%d, %d)", vetorAresta->vetor[i]->verticeO, vetorAresta->vetor[i]->verticeD);
    }
    printf("\n");
}

void mostrarGrafo(Grafo *grafo){
    int i;
    for(i = 0; i <= grafo->qtdVertices; i++){
        printf("%d: ", i);
        struct no *temp = grafo->vetorArestas[i];
        while(temp != NULL){
            printf("(%d, %d)", temp->aresta->verticeO, temp->aresta->verticeD);
            temp = temp->prox;
        }
        printf("\n");
    }
}



int calcularPesoTotal(VetorAresta *vetorAresta){
    int pesoTotal = 0;
    int i;
    for(i = 0; i < vetorAresta->contador; i++ ){
        pesoTotal = pesoTotal + vetorAresta->vetor[i]->peso;
    }
    return pesoTotal;
}


void entradaTXT(Grafo *grafo, char arquivo[]){
    FILE *arquivoLeitura = fopen(arquivo, "r");
    if(arquivoLeitura == NULL){
        printf("erro na entrada\n");
        exit(0);
    }
    else{
    	int numeroVertice, numeroAresta;
        fscanf(arquivoLeitura, "%d %d\n", &numeroVertice, &numeroAresta);
       	comecarGrafo(grafo, numeroAresta, numeroVertice);
        int i;
        for(i = 0; i < numeroAresta; i++){
        	int origem, destino, peso;
       		fscanf(arquivoLeitura, "%d %d %d\n", &origem, &destino, &peso);
            adicionarAresta(grafo, origem, destino, peso);
       
        }

    }
    fclose(arquivoLeitura);            
}

void saidaTXT(VetorAresta *vetorAresta, FILE *arquivo){
	selectionSortArestas(vetorAresta);
	int i;
	for (i = 0; i < vetorAresta->contador; ++i)
	{
		fprintf(arquivo, "(%d, %d)", vetorAresta->vetor[i]->verticeO, vetorAresta->vetor[i]->verticeD);
	}
}

/*
int main(int argc, char const *argv[]){
	//Grafo grafoInicial;
	//Grafo grafoFinal;
	//VetorAresta vetorAresta;
	//VetorVertice vetorVertice;
	//comecarVetorVertices(&vetorVertice);
	//incializarGrafo(&grafoFinal, 9, 10);
	//incializarGrafo(&grafoInicial, 45, 10);
	/*
	adicionarAresta(&grafoInicial, 0, 1, 1);
	adicionarAresta(&grafoInicial, 0, 3, 6);
	adicionarAresta(&grafoInicial, 0, 2, 6);
	adicionarAresta(&grafoInicial, 1, 3, 2);
	adicionarAresta(&grafoInicial, 1, 2, 2);
	adicionarAresta(&grafoInicial, 3, 4, 5);
	adicionarAresta(&grafoInicial, 1, 4, 5);
	adicionarAresta(&grafoInicial, 1, 5, 4);
	adicionarAresta(&grafoInicial, 2, 5, 4);
	adicionarAresta(&grafoInicial, 4, 5, 3);
	
	adicionarAresta(&grafoInicial, 1, 2, 4);
	adicionarAresta(&grafoInicial, 1, 3, 1);
	adicionarAresta(&grafoInicial, 1, 6, 3);
	adicionarAresta(&grafoInicial, 3, 5, 4);
	adicionarAresta(&grafoInicial, 2, 3, 4);
	adicionarAresta(&grafoInicial, 2, 6, 4);
	adicionarAresta(&grafoInicial, 3, 6, 2);
	adicionarAresta(&grafoInicial, 4, 5, 3);
	adicionarAresta(&grafoInicial, 5, 6, 6);
	
	adicionarAresta(&grafoInicial, 1, 2, 5);
	adicionarAresta(&grafoInicial, 1, 3, 4);
	adicionarAresta(&grafoInicial, 1, 4, 2);
	adicionarAresta(&grafoInicial, 1, 6, 6);
	adicionarAresta(&grafoInicial, 2, 4, 1);
	adicionarAresta(&grafoInicial, 2, 5, 7);
	adicionarAresta(&grafoInicial, 3, 5, 6);
	adicionarAresta(&grafoInicial, 4, 6, 1);

	
	//comecarVetorArestas(&vetorAresta, 0);
	//criarVetorArestas(&grafoInicial, &vetorAresta);
	//criarVetorVertices(&grafoInicial, &vetorVertice);
	//selectionSort(&vetorAresta);
	//mostrarVetorArestas(&vetorAresta);
	
	//removerAresta(&vetorAresta, 4, 5);
	//mostrarVetorArestas(&vetorAresta);
	//Prim(&grafoInicial, &grafoFinal, &vetorAresta);
	//	mostrarGrafo(&grafoFinal);
	//criarVetorArestas(&grafoFinal, &vetorAresta);
	//mostrarGrafo(&grafoFinal);
	//mostrarVetorArestas(&vetorAresta);
	//removerVertice(&vetorVertice, 3);
	//removerVertice(&vetorVertice, 6);
	//int resultado = verificacaoDeVertice2(&vetorVertice, 0, 6);
	//printf("%d\n", resultado);
	//mostrarVetorVertice(&vetorVertice);
	//entradaTXT(&grafoInicial, "G3-85.dat");
	//Prim(&grafoInicial, &grafoFinal, &vetorAresta);
	//mostrarGrafo(&grafoFinal);
		
	return 0;
}

*/


















