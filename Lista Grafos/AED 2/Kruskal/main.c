#include "kruskal.h"

int main(int argv, char *argc[]){
	Grafo *grafoI = comecarGrafo(0, 1);
    Grafo *grafoF = comecarGrafo(0, 1);
    int i;
    int flag = 0;
    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                    -  Menu de ajuda\n");
            printf("\n-f Recebe arquivo.dat -  Arquivo de entrada\n");
            printf("\n-o Recebe arquivo.dat -  Arquivo de saida\n" );
            printf("\n-s Recebe inteiro     -  mostra a solução (em ordem crescente)\n");
        }
        if(strcmp(argc[i], "-f")== 0){
            entradaTXT(grafoI, argc[i+1]);
            Kruskal(grafoI, grafoF);
            flag = 1;
        } 
        if(strcmp(argc[i], "-o") == 0){
        	FILE *arquivo = fopen(argc[i+1], "w");
        	if (arquivo == NULL){
        		printf("ERRO - Arquivo de saida invalido\n");
        	}
        	else{
        		saidaTXT(grafoF, arquivo);
        	}
            fclose(arquivo); 
            flag = 1;
        }
        if(strcmp(argc[i], "-s")== 0){
            mostrarGrafo(grafoF);       
            flag = 0;
        } 
    }
    if (flag == 1){
        Aresta *temp = grafoF->arestas;
        int peso = 0;
        while(temp != NULL){
            peso += temp -> peso;
            temp= temp->prox;
        }
        printf("%d\n", peso);
    }
    return 0;
}

