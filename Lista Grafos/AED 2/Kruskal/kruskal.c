#include "kruskal.h"

Aresta* comecarAresta(){
	return NULL;
}

void bubbleSort(Aresta* l){
	Aresta* auxiliar;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(auxiliar = l; auxiliar->prox!=NULL; auxiliar = auxiliar->prox){
			if(auxiliar->peso > auxiliar->prox->peso){
				tempP = auxiliar->peso;
				auxiliar->peso = auxiliar->prox->peso;
				auxiliar->prox->peso = tempP;
				
				tempV1 = auxiliar->verticeO;
				auxiliar->verticeO = auxiliar->prox->verticeO;
				auxiliar->prox->verticeO = tempV1;
				
				tempV2 = auxiliar->verticeD;
				auxiliar->verticeD = auxiliar->prox->verticeD;
				auxiliar->prox->verticeD = tempV2;
				trocou = 0;
			}	
		}
	}
}

void bubbleSortAresta(Aresta* l){
	Aresta* auxiliar;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(auxiliar = l; auxiliar->prox!=NULL; auxiliar = auxiliar->prox){
			if(auxiliar->verticeO > auxiliar->prox->verticeO){
				tempP = auxiliar->peso;
				auxiliar->peso = auxiliar->prox->peso;
				auxiliar->prox->peso = tempP;
				
				tempV1 = auxiliar->verticeO;
				auxiliar->verticeO = auxiliar->prox->verticeO;
				auxiliar->prox->verticeO = tempV1;
				
				tempV2 = auxiliar->verticeD;
				auxiliar->verticeD = auxiliar->prox->verticeD;
				auxiliar->prox->verticeD = tempV2;
				trocou = 0;
			}	
		}
	}
}

Aresta* adicionarAresta(Aresta* l, int verticeO, int verticeD, int peso){
	Aresta* novaAresta = (Aresta*)malloc(sizeof(Aresta));
	novaAresta->verticeO = verticeO;
	novaAresta->verticeD = verticeD;
	novaAresta->peso = peso;
	if(l==NULL){
		novaAresta->prox = NULL;
		return novaAresta;
	}
	novaAresta->prox = l;
	return novaAresta;
}


Grafo *comecarGrafo(int ponderado, int digrafo)
{
    Grafo *grafo = (Grafo*)malloc(sizeof(Grafo));
    Aresta *arestas = (Aresta*)malloc(sizeof(Aresta));
    grafo->arestas = arestas;
    int i, j;
    for(i = 0; i < MAXIMOVERTICE; i++){
        for(j = 0; j < MAXIMOVERTICE; j++){
            grafo->matriz[i][j] = 0;
        }
    }
    grafo->digrafo = digrafo;
    grafo->ponderado = ponderado;

    
    return grafo;
}


void adicionarGrafo(Grafo *grafo, int verticeO, int verticeD, int peso)
{
    grafo->arestas = adicionarAresta(grafo->arestas, verticeO, verticeD, peso);

    if(grafo->ponderado == 0 && grafo->digrafo == 0)
    {
        grafo->matriz[verticeO][verticeD] = peso;
    }

    else if(grafo->ponderado == 0 && grafo->digrafo == 0)
    {
        grafo->matriz[verticeO][verticeD] = peso;
        grafo->matriz[verticeD][verticeO] = peso;
    }

    else if(grafo->ponderado == 0 && grafo->digrafo == 0)
    {
        grafo->matriz[verticeO][verticeD] = 1;
    }

    else if(grafo->ponderado == 0 && grafo->digrafo == 0)
    {
        grafo->matriz[verticeO][verticeD] = 1;
        grafo->matriz[verticeD][verticeO] = 1;
    }
}


void Kruskal(Grafo *grafo, Grafo *grafoF)
{
    bubbleSort(grafo->arestas);
    Aresta *temp;
    int vetorAuxiliar[MAXIMOVERTICE];
    int peso, peso1, peso2;
    int i;
    for(i = 0; i < MAXIMOVERTICE; i++)
    {
        vetorAuxiliar[i] = i;
    }

    peso = 0;

    for(temp = grafo->arestas; temp != NULL; temp = temp->prox)
    {
        if(vetorAuxiliar[temp->verticeO] != vetorAuxiliar[temp->verticeD])
        {
            peso1 = vetorAuxiliar[temp->verticeO];
            peso2 = vetorAuxiliar[temp->verticeD];
        
        for(int j = 0; j < MAXIMOVERTICE; j++)
        {
            if(vetorAuxiliar[j] == peso2)
            {
                vetorAuxiliar[j] = peso1;
            }
        }
        adicionarGrafo(grafoF, temp->verticeO, temp->verticeD, temp->peso);
        peso = peso + temp->peso;
       
       }
    }

}




void mostrarGrafo(Grafo *grafo){
	bubbleSortAresta(grafo->arestas);
	Aresta *temp = grafo->arestas -> prox;
	while(temp != NULL){
		printf("(%d,%d) ", temp->verticeO, temp->verticeD);
		temp= temp->prox;
	}
	printf("\n");
}

int calcularPesoGrafo(Grafo *grafo){
	Aresta *temp = grafo->arestas -> prox;
	int peso = 0;
	while(temp != NULL){
		peso += temp -> peso;
		temp= temp->prox;
	}
	return peso;
}

void entradaTXT(Grafo *grafo, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int vertices, arestas;
        fscanf(abrirArquivo, "%d %d\n", &vertices, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            adicionarGrafo(grafo, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaTXT(Grafo *grafo, FILE *arquivo){
	bubbleSortAresta(grafo->arestas);
	Aresta *temp = grafo->arestas->prox;   
	while(temp != NULL){
		fprintf(arquivo, "(%d,%d) ", temp->verticeO, temp->verticeD);
		temp= temp->prox;
	}
}


/*
int main(int argc, char ** argv){
	Grafo* grafo = comecarGrafo(0, 1);
	Grafo* grafoF = comecarGrafo(0, 1);
	/*
	adicionarGrafo(grafo, 1, 2, 5);
	adicionarGrafo(grafo, 1, 3, 4);
	adicionarGrafo(grafo, 1, 4, 2);
	adicionarGrafo(grafo, 1, 6, 6);
	adicionarGrafo(grafo, 2, 4, 1);
	adicionarGrafo(grafo, 2, 5, 7);
	adicionarGrafo(grafo, 3, 5, 6);
	adicionarGrafo(grafo, 4, 6, 1);	
	
	entradaTXT(grafo, "G1-71.dat");
	Kruskal(grafo, grafoF);
	mostrarGrafo(grafoF);
	FILE *arquivo = fopen("saida.dat", "w");
        	if (arquivo == NULL){
        		printf("ERRO - Arquivo de saida invalido\n");
        	}
        	else{
        		saidaTXT(grafoF, arquivo);
        	} 
    printf("%d\n", calcularPesoGrafo(grafoF));


}

*/