#ifndef _KRUSKAL_H
#define _KRUSKAL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define MAXIMOVERTICE 200


typedef struct Aresta{
	int verticeO;
	int verticeD;
	int peso;
	struct Aresta* prox;
} Aresta;


typedef struct Grafo{
    int ponderado;
    int digrafo;
    int matriz[MAXIMOVERTICE][MAXIMOVERTICE];
    Aresta *arestas;
}Grafo;

Aresta* comecarAresta();
void bubbleSort(Aresta* l);
void bubbleSortAresta(Aresta* l);
Aresta* adicionarAresta(Aresta* l, int verticeO, int verticeD, int peso);
Grafo *comecarGrafo(int ponderado, int digrafo);
void adicionarGrafo(Grafo *grafo, int verticeO, int verticeD, int peso);
void Kruskal(Grafo *grafo, Grafo *grafoFinal);
void mostrarGrafo(Grafo *grafo);
int calcularPesoGrafo(Grafo *grafo);
void entradaTXT(Grafo *grafo, char arquivo[]);
void saidaTXT(Grafo *grafo, FILE *arquivo);

#endif