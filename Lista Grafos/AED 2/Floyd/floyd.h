#ifndef __Djikstra__
#define __Djikstra__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VERTMAX 50
#define ARESTAMAX 20
#define true 1
#define false 0

typedef struct lista{
	int vertice1;
	int vertice2;
	int peso;
	struct lista* prox;
} Lista;

typedef struct __GRAFO__
{
    int ponderado;
    int digrafo;
    int matriz[VERTMAX][VERTMAX];
    Lista *arestas;
}Grafo;

typedef struct listaAux
{
    int vertice;
    int custo;
    int prev;
}ListaAux;

Lista* criarLista();
void bubbleSort(Lista* l);
Lista* adicionarLista(Lista* l, int vertice1, int vertice2, int peso);
Grafo *comecarGrafo(int ponderado, int digrafo);
void adicionarGrafo(Grafo *grafo, int vertice1, int vertice2, int peso);
void FloydWarshall(Grafo *grafo, Grafo *grafoFinal);
int Numero_Vertice(Grafo *grafo);
void Imprimir(Grafo *grafo, int verticeInicial);
void entradaTXT(Grafo *grafo, char arquivo[]);
void saidaTXT(Grafo *grafo, int verticeInicial, FILE *arquivo);


#endif