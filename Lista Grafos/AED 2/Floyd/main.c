#include "floyd.h"


int main(int argv, char *argc[]){
	Grafo *grafoI = comecarGrafo(true, true);
    Grafo *grafoF = comecarGrafo(true, true);
    int i;
    int flag = 0;
    int flagPrintEspecifico = 0;
    int verticeInicial = 0;
    int verticeFinal = 0;
    int flagSalvar = 0;
    char arquivoSaida[100];

    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                    -  Menu de ajuda\n");
            printf("\n-f Recebe arquivo.dat -  Arquivo de entrada\n");
            printf("\n-o Recebe arquivo.dat -  Arquivo de saida\n" );
            printf("\n-i Recebe inteiro     -  vértice inicial (obrigatório)\n");
            printf("\n-l Recebe inteiro     -  vértice final (opcional)\n");

        }
        if(strcmp(argc[i], "-f")== 0){
            entradaTXT(grafoI, argc[i+1]);
            flag = 1;
        } 
        if(strcmp(argc[i], "-i")== 0){
            FloydWarshall(grafoI, grafoF);
            verticeInicial = atoi(argc[i+1]);

            flag = 1;
        } 
        if(strcmp(argc[i], "-o") == 0){
            strcpy(arquivoSaida, argc[i+1]);
        	flagSalvar = 1;
            flag = 1;
        }
        if(strcmp(argc[i], "-l")== 0){       
            flagPrintEspecifico = 1;
            verticeFinal = atoi(argc[i+1]);
        } 
    }
    if (flagPrintEspecifico == 1){
        ImprimirEspecifico(grafoF, verticeInicial, verticeFinal);
    }
    else{
        Imprimir(grafoF, verticeInicial);
    }
    if(flagSalvar == 1){
        FILE *arquivo = fopen(arquivoSaida, "w");
        if (arquivo == NULL){
            printf("ERRO - Arquivo de saida invalido\n");
        }
        else{
            saidaTXT(grafoF, verticeInicial, arquivo);
        }
        fclose(arquivo); 
    }
    return 0;
}
    
