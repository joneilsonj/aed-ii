#include "floyd.h"
#include "Heap.c"

Lista* criarLista(){
	return NULL;
}

void bubbleSort(Lista* l){
	Lista* aux;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(aux = l; aux->prox!=NULL; aux = aux->prox){
			if(aux->peso > aux->prox->peso){
				tempP = aux->peso;
				aux->peso = aux->prox->peso;
				aux->prox->peso = tempP;
				
				tempV1 = aux->vertice1;
				aux->vertice1 = aux->prox->vertice1;
				aux->prox->vertice1 = tempV1;
				
				tempV2 = aux->vertice2;
				aux->vertice2 = aux->prox->vertice2;
				aux->prox->vertice2 = tempV2;
				trocou = 0;
			}	
		}
	}
}

Lista* adicionarLista(Lista* l, int vertice1, int vertice2, int peso){
	Lista* novo = (Lista*)malloc(sizeof(Lista));
	novo->vertice1 = vertice1;
	novo->vertice2 = vertice2;
	novo->peso = peso;
	if(l==NULL){
		novo->prox = NULL;
		return novo;
	}
	novo->prox = l;
	return novo;
}

Grafo *comecarGrafo(int ponderado, int digrafo)
{
    Grafo *grafo = (Grafo*)malloc(sizeof(Grafo));
    Lista *arestas = (Lista*)malloc(sizeof(Lista));
    grafo->arestas = arestas;
    int i, j;
    for(i = 0; i < VERTMAX; i++){
        for(j = 0; j < VERTMAX; j++){
            grafo->matriz[i][j] = 0;
        }
    }
    grafo->digrafo = digrafo;
    grafo->ponderado = ponderado;

    
    return grafo;
}


void adicionarGrafo(Grafo *grafo, int vertice1, int vertice2, int peso)
{
    grafo->arestas = adicionarLista(grafo->arestas, vertice1, vertice2, peso);

    if(grafo->ponderado == true && grafo->digrafo == true)
    {
        grafo->matriz[vertice1][vertice2] = peso;
    }

    else if(grafo->ponderado == true && grafo->digrafo == false)
    {
        grafo->matriz[vertice1][vertice2] = peso;
        grafo->matriz[vertice2][vertice1] = peso;
    }

    else if(grafo->ponderado == false && grafo->digrafo == true)
    {
        grafo->matriz[vertice1][vertice2] = 1;
    }

    else if(grafo->ponderado == false && grafo->digrafo == false)
    {
        grafo->matriz[vertice1][vertice2] = 1;
        grafo->matriz[vertice2][vertice1] = 1;
    }
}



void FloydWarshall(Grafo *grafo, Grafo *grafoFinal)
{
    int floyd[VERTMAX][VERTMAX];
    int i, j, k;


    for(i = 0; i < VERTMAX; i++)
    {
        for(j = 0; j < VERTMAX; j++)
        {
            floyd[i][j] = grafo->matriz[i][j];
            if(floyd[i][j] == 0)
            {
                floyd[i][j] = 99999;
            }
        }
    }
    for(k = 0; k < VERTMAX; k++)
    {
        for(i = 0; i < VERTMAX; i++)
        {
            for(j = 0; j < VERTMAX; j++)
            {
                if(i == j)
                {
                    floyd[i][j] = 0;
                }
                else if(floyd[i][k] + floyd[k][j] < floyd[i][j])
                {
                    floyd[i][j] = floyd[i][k] + floyd[k][j];
                }
            }
        }
    }
    for(i = 1; i <= (Numero_Vertice(grafo)); i++ )
    {
        for(j = 1; j <=(Numero_Vertice(grafo)); j++)
        {
            if(floyd[i][j] != 99999)
            {
                adicionarGrafo(grafoFinal, i, j, floyd[i][j]);
            }
        }
        //printf("\n");
    }
}

int Numero_Vertice(Grafo *grafo)
{
    int maior;
    maior = grafo->arestas->vertice1;
    Lista *temp;

    for(temp = grafo->arestas; temp != NULL; temp = temp->prox)
    {
        if(temp->vertice1 > maior)
        {
            maior = temp->vertice1;
        }
        else if(temp->vertice2 > maior)
        {
            maior = temp->vertice2;
        }
    }
    return maior;
}

void ImprimirEspecifico(Grafo *grafo, int inicial, int final)
{
    if(grafo->matriz[inicial][final] != 0 && grafo->ponderado == false)
    {
        printf("%d", grafo->matriz[inicial][final]);
    }
    else if(grafo->matriz[inicial][final] != 0 && grafo->ponderado == true)
    {
        printf("%d", grafo->matriz[inicial][final]);
    }
    printf("\n");
}

void Imprimir(Grafo *grafo, int verticeInicial)
{
    int i, j;
    printf("(%d:%d) ", verticeInicial, 0);
        for(j = 0; j < VERTMAX; j++)
        {
            if(grafo->matriz[verticeInicial][j] != 0 && grafo->ponderado == false)
            {
                if(verticeInicial != j){

                    printf("(%d:%d) ",j, grafo->matriz[verticeInicial][j]);
                }
            }
            else if(grafo->matriz[verticeInicial][j] != 0 && grafo->ponderado == true)
            {   
                if (verticeInicial != j)
                 {
                     printf("(%d:%d) " ,j,grafo->matriz[verticeInicial][j]);
                 } 
            }
        }
    printf("\n");
    
}

void entradaTXT(Grafo *grafo, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int vertices, arestas;
        fscanf(abrirArquivo, "%d %d\n", &vertices, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            adicionarGrafo(grafo, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaTXT(Grafo *grafo, int verticeInicial, FILE *arquivo){
    int i, j;
    fprintf(arquivo, "(%d:%d) ", verticeInicial, 0);
        for(j = 0; j < VERTMAX; j++)
        {
            if(grafo->matriz[verticeInicial][j] != 0 && grafo->ponderado == false)
            {
                if(verticeInicial != j){

                    fprintf(arquivo, "(%d:%d) ",j, grafo->matriz[verticeInicial][j]);
                }
            }
            else if(grafo->matriz[verticeInicial][j] != 0 && grafo->ponderado == true)
            {   
                if (verticeInicial != j)
                 {
                     fprintf(arquivo, "(%d:%d) " ,j,grafo->matriz[verticeInicial][j]);
                 } 
            }
        }
    fprintf(arquivo, "\n");
}

/*
int main(int argc, char ** argv){
    Grafo* grafo = comecarGrafo(true, false);
    Grafo* grafoFinal = comecarGrafo(true, false);
	/*adicionarGrafo(grafo, 1, 2, 5);
	adicionarGrafo(grafo, 1, 3, 4);
	adicionarGrafo(grafo, 1, 4, 2);
	adicionarGrafo(grafo, 1, 6, 6);
	adicionarGrafo(grafo, 2, 4, 1);
	adicionarGrafo(grafo, 2, 5, 7);
	adicionarGrafo(grafo, 3, 5, 6);
	adicionarGrafo(grafo, 4, 6, 1);	
	//adicionarGrafo(grafo, 5, 6, 5);
	//adicionarGrafo(grafo, 6, 7, 5);
	printf("\nGRAFO: \n");
  entradaTXT(grafo, "entrada.dat");
    Imprimir(grafo, 1);
	printf("\nFLOYD-WARSHALL - CAMINHO MINIMO ENTRE DOIS VERTICES DO GRAFO - MATRIZ: \n");
	FloydWarshall(grafo, grafoFinal);
    Imprimir(grafoFinal, 1);
    FILE *abrirArquivo = fopen("saida.dat", "w");
    saidaTXT(grafoFinal, 1, abrirArquivo);
    ImprimirEspecifico(grafoFinal, 1, 5);
}


*/