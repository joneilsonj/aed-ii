#include "dijkstra.h"
#include "Heap.c"

Lista* criarLista(){
	return NULL;
}

void bubbleSort(Lista* l){
	Lista* aux;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(aux = l; aux->prox!=NULL; aux = aux->prox){
			if(aux->peso > aux->prox->peso){
				tempP = aux->peso;
				aux->peso = aux->prox->peso;
				aux->prox->peso = tempP;
				
				tempV1 = aux->vertice1;
				aux->vertice1 = aux->prox->vertice1;
				aux->prox->vertice1 = tempV1;
				
				tempV2 = aux->vertice2;
				aux->vertice2 = aux->prox->vertice2;
				aux->prox->vertice2 = tempV2;
				trocou = 0;
			}	
		}
	}
}

Lista* inserirLista(Lista* l, int vertice1, int vertice2, int peso){
	Lista* novo = (Lista*)malloc(sizeof(Lista));
	novo->vertice1 = vertice1;
	novo->vertice2 = vertice2;
	novo->peso = peso;
	if(l==NULL){
		novo->prox = NULL;
		return novo;
	}
	novo->prox = l;
	return novo;
}

Grafo *comecarGrafo(int ponderado, int digrafo)
{
    Grafo *grafo = (Grafo*)malloc(sizeof(Grafo));
    //Lista de arestas
    Lista *arestas = (Lista*)malloc(sizeof(Lista));
    grafo->arestas = arestas;
    int i, j;
    for(i = 0; i < VERTMAX; i++){
        for(j = 0; j < VERTMAX; j++){
            grafo->matriz[i][j] = 0;
        }
    }
    grafo->digrafo = digrafo;
    grafo->ponderado = ponderado;

    
    return grafo;
}


void adicionarGrafo(Grafo *grafo, int vertice1, int vertice2, int peso)
{
    grafo->arestas = inserirLista(grafo->arestas, vertice1, vertice2, peso);

    if(grafo->ponderado == true && grafo->digrafo == true)
    {
        grafo->matriz[vertice1][vertice2] = peso;
    }

    else if(grafo->ponderado == true && grafo->digrafo == false)
    {
        grafo->matriz[vertice1][vertice2] = peso;
        grafo->matriz[vertice2][vertice1] = peso;
    }

    else if(grafo->ponderado == false && grafo->digrafo == true)
    {
        grafo->matriz[vertice1][vertice2] = 1;
    }

    else if(grafo->ponderado == false && grafo->digrafo == false)
    {
        grafo->matriz[vertice1][vertice2] = 1;
        grafo->matriz[vertice2][vertice1] = 1;
    }
}



void Djikstra(Grafo *grafo, Grafo *grafoFinal, int v_inicial)
{
    bubbleSort(grafo->arestas);
    int escolha;
    Heap *prioridade = (Heap*)malloc(sizeof(Heap));
    Lista *aux;
    ListaAux *geradora[VERTMAX];
    ListaAux *min;

    for(int i = 0; i < VERTMAX; i++)
    {
        geradora[i] = (ListaAux*)malloc(sizeof(geradora));
        geradora[i]->vertice = i;
        geradora[i]->custo = 99999;
        geradora[i]->prev = -1;
        if(i == v_inicial)
        {
            geradora[i]->custo = 0;
        }
        //printf"Chamad da função do heap"
        min_heap_insert(prioridade, geradora[i]);
    }
    build_min_heap(prioridade);
    while(heap_vazia(prioridade) != 1)
    {
        min = heap_extract_min(prioridade);
        for(aux = grafo->arestas->prox; aux != NULL; aux = aux->prox)
        {
            if(aux->vertice1 == min->vertice || aux->vertice2 == min->vertice)
            {
                if(aux->vertice1 == min->vertice)
                {
                    escolha = aux->vertice2;
                }
                else if(aux->vertice2 == min->vertice)
                {
                    escolha = aux->vertice1;
                }
                if(geradora[escolha]->custo > min->custo + aux->peso)
                {
                    geradora[escolha]->custo = min->custo + aux->peso;
                    geradora[escolha]->prev = min->vertice;
                    build_min_heap(prioridade);
                }
            }
        }
        if(min->prev != -1){
			//printf("Menor caminho do vértice [%d] até o vertice [%d] tem distancia: %d\n",v_inicial, min->vertice, min->custo);
            adicionarGrafo(grafoFinal, v_inicial, min->vertice, min->custo);
        }
    }


}



void Imprimir(Grafo *grafo, int verticeInicial)
{
    int i, j;
    printf("%d:%d ", verticeInicial, 0);
    for(i = 0; i < VERTMAX; i++)
    {
        for(j = 0; j < VERTMAX; j++)
        {
            if(grafo->matriz[i][j] != 0 && grafo->ponderado == false)
            {
                printf("%d:%d ",j, grafo->matriz[i][j]);
            }
            else if(grafo->matriz[i][j] != 0 && grafo->ponderado == true)
            {
                printf("%d:%d ",j,grafo->matriz[i][j]);
            }
        }
    }
    printf("\n");
}

void ImprimirEspecifico(Grafo *grafo, int inicial, int final)
{
    if(grafo->matriz[inicial][final] != 0 && grafo->ponderado == false)
    {
        printf("%d", grafo->matriz[inicial][final]);
    }
    else if(grafo->matriz[inicial][final] != 0 && grafo->ponderado == true)
    {
        printf("%d", grafo->matriz[inicial][final]);
    }
    printf("\n");
}

void entradaTXT(Grafo *grafo, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int vertices, arestas;
        fscanf(abrirArquivo, "%d %d\n", &vertices, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            adicionarGrafo(grafo, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaTXT(Grafo *grafo, int verticeInicial, FILE *arquivo){
    int i, j;
    fprintf(arquivo, "%d:%d ", verticeInicial, 0);
    for(i = 0; i < VERTMAX; i++)
    {
        for(j = 0; j < VERTMAX; j++)
        {
            if(grafo->matriz[i][j] != 0 && grafo->ponderado == true)
            {
                fprintf(arquivo, "%d:%d ",j, grafo->matriz[i][j]);
            }
            else if(grafo->matriz[i][j] != 0 && grafo->ponderado == true)
            {
                fprintf(arquivo, "%d:%d ",j,grafo->matriz[i][j]);
            }
        }
    }
    fprintf(arquivo, "\n");
}

/*
int main(int argc, char ** argv){
    Grafo* grafo = comecarGrafo(true, true);
    Grafo* grafoFinal = comecarGrafo(true, true);
	adicionarGrafo(grafo, 1, 2, 5);
	adicionarGrafo(grafo, 1, 3, 4);
	adicionarGrafo(grafo, 1, 4, 2);
	adicionarGrafo(grafo, 1, 6, 6);
	adicionarGrafo(grafo, 2, 4, 1);
	adicionarGrafo(grafo, 2, 5, 7);
	adicionarGrafo(grafo, 3, 5, 6);
	adicionarGrafo(grafo, 4, 6, 1);	
	//adicionarGrafo(grafo, 5, 6, 5);
	//adicionarGrafo(grafo, 6, 7, 5);
	printf("\nGRAFO: \n");
	Imprimir(grafo);
	printf("\nDJIKSTRA - CAMINHO MINIMO ENTRE DOIS VERTICES DO GRAFO: \n");
	Djikstra(grafo, grafoFinal,4);
    printf("\n\n\n\n\n\n\n\n");
    Imprimir(grafoFinal);
    ImprimirEspecifico(grafoFinal, 4, 3);
    
}

*/  
